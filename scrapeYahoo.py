import pyautogui
import os, sys, csv
import time

def getListOfStocks(listOfStocks):
	return listOfStocks

def whileOnPage():
	pyautogui.dragTo(2269,1322)
	pyautogui.click()
	time.sleep(1)
	pyautogui.dragTo(2461,1457)
	pyautogui.click()
	time.sleep(1)
	pyautogui.dragTo(2171,1727)
	pyautogui.click()

	time.sleep(2)
	pyautogui.dragTo(3035, 1408, 1)
	pyautogui.click()

	time.sleep(2)
	pyautogui.dragTo(3056,1488, 1)
	pyautogui.click()
	time.sleep(3)

def goToNextPage(symbol):
	url = f"https://finance.yahoo.com/quote/{symbol}/history?interval=1d&filter=history&frequency=1d"
	pyautogui.dragTo(2817,168)
	pyautogui.click()

	pyautogui.typewrite(url + "\n")


if __name__ == '__main__':
	stocks = ['MMM','AXP','AAPL','BA','CAT','CVX','CSCO','KO','DIS','XOM','GS','HD','IBM','INTC','JNJ','JPM','MCD','MRK','MSFT','NKE','PFE','PG','TRV','UTX','UNH','VZ','V','WMT','WBA']
	try:
		for item in stocks:
			goToNextPage(item)
			time.sleep(1)
			whileOnPage()
	except KeyboardInterrupt:
		pass
