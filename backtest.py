import csv
import sys
import os
import datetime
from datetime import datetime, timedelta
import pandas as pd

def getPrices():
	path = 'prices'
	files = os.listdir(path)
	for name in files:
		print('loaded \t' + name)

	return files

def getCSV(path, fromDate, toDate):
	'''
	how to call
	getCSV('prices/AAPL.csv', datetime(1980,12,12), datetime(1981,2,19))
	'''

	with open(path) as csv_file:
		csv_reader = csv.reader(csv_file,delimiter=',')
		line_count = 0
		close_arr = []

		df = [{'name' : path.split('/')[1]}]

		for row in csv_reader:
			if line_count == 0:
				#print(f'column names are {", ".join(row)}')
				line_count += 1
			else:
				temp = row[0].split('-')
				calcDate = datetime(int(temp[0]),int(temp[1]),int(temp[2]))
				if(calcDate >= fromDate and calcDate <= toDate):
					newEntry = {'date': calcDate, 'price':row[4]}
					df.append(newEntry)

		return df


class Backtest:
	startDate = None
	endDate = None
	stocksList = []
	stocksMaster = []
	portfolio = []

	currentDate = None
	money = 0

	def __init__(self, startDate, endDate, money, stocks):
		self.startDate = startDate
		self.currentDate = startDate
		self.endDate = endDate
		self.money = money
		self.stocksList = stocks

		for item in self.stocksList:
			result = getCSV('prices/'+item+'.csv', startDate, endDate)
			self.stocksMaster.append(result)

		for item in self.stocksList:
			self.portfolio.append({'symbol': item, 'shares': 0})

	def getCurrentPrice(self):
		closing = 0
		for item in stocksMaster:
			if(item[0]['name'] == symbol):
				for element in item[0]: 
					if element['date'] == self.currentDate:
						closing = element['price']
		return closing

	def insertPortfolio(symbol, shares):
		for item in portfolio:
			if item['symbol'] == symbol:
				item['shares'] += shares

		return True

	def removePortfolio(symbol, shares):
		for item in portfolio:
			if item['symbol'] == symbol:
				if(item['shares'] >= shares):
					item['shares'] -= shares
					return True
				else:
					return False

	def buy(self, shares, symbol):
		closing = getCurrentPrice()

		if self.money >= shares * closing:
			insertPortfolio(symbol, shares)
			return True
		else:
			return False

	def sell(self, shares, symbol):
		closing = getCurrentPrice()
		if(removePortfolio(symbol, shares)):
			self.money += closing * shares
			return True
		else:
			return False

	def getInfo(self, symbol):
		resultant = []
		for item in self.stocksMaster:
			if item[0]['name'] == (symbol + '.csv'):
				count = 0
				for element in item:
					if count != 0:
						if(element['date'] <= self.currentDate):
							resultant.append(element)

						else:
							break
					count += 1

		return resultant

	def toDataFrame(self, inputVar):
		d = {'date': [i['date'] for i in inputVar], 'price':[i['price'] for i in inputVar]}
		df = pd.DataFrame(data=d)
		return df

	def endTurn(self):
		if(self.currentDate + timedelta(days=1) > self.endDate):
			return False

		self.currentDate += timedelta(days=1)
		return True

bckObj = Backtest(datetime(2014, 1, 1), datetime(2015,1,1), 30000,
['MMM','AXP','AAPL','BA','CAT','CVX','CSCO','KO','DIS','XOM','GS','HD','IBM','INTC','JNJ','JPM','MCD','MRK','MSFT','NKE','PFE','PG','TRV','UTX','UNH','VZ','V','WMT','WBA'])

bckObj.endTurn()
bckObj.endTurn()
print(bckObj.toDataFrame(bckObj.getInfo('MSFT')))